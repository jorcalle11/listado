package com.ssg.listado;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends Activity {

    private static final ArrayList <String> names = new ArrayList<String>();
    public static final String NAME_TAG = "name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        names.add("Jaimito");
        final EditText inputName = (EditText) findViewById(R.id.editName);
        ListView list = (ListView) findViewById(R.id.list_of_names);
        Button btnSubmit = (Button) findViewById(R.id.buttonSubmit);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1, names);


        list.setAdapter(adapter);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = inputName.getText().toString();
                inputName.setText("");
                Log.i(NAME_TAG, name);
                if (!names.contains(name)) {
                    names.add(name);
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }
}
