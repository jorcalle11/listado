package com.ssg.listado;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;


/**
 * Created by jorge on 19/02/16.
 */
public class NameDetailActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_detail);

        Intent i = getIntent();
        String name = i.getStringExtra(MainActivity.NAME_TAG);

        android.app.FragmentManager fm = getFragmentManager();
        NameDetailFragment frag = (NameDetailFragment) fm.findFragmentById(R.id.detailFragment);
        frag.setName(name);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

}